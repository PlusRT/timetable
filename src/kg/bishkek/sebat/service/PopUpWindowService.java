package kg.bishkek.sebat.service;

/**
 * Created by temirlan on 22.06.2017.
 */
public interface PopUpWindowService<T> extends WindowService {
    void setItemOnClick(T t);
}