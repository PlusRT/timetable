package kg.bishkek.sebat.service;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.List;

/**
 * Created by temirlan on 19.06.2017.
 */
public interface TableService {

    List<TableView> getTableViews();

    List<TableColumn> addColumns(TableView tableView, int columnAmount, String... columnsName);

    List<TableColumn> getColumns();

    List<TableColumn> addColumns(TableColumn tableColumn, int columnAmount, String... columnsName);

}
