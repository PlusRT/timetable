package kg.bishkek.sebat.service;

import java.util.Locale;

/**
 * Created by temirlan on 28.06.2017.
 */
public interface FXMLService {

    <T> T load(String path);

    <T> T load(String path, Locale locale);
}
