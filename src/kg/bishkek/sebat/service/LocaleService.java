package kg.bishkek.sebat.service;

import java.util.Locale;

/**
 * Created by temirlan on 23.06.2017.
 */
public interface LocaleService {
    void changeLocale(Locale newLocale);

    Locale getLocale();
}
