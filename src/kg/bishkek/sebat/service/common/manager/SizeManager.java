package kg.bishkek.sebat.service.common.manager;

/**
 * Created by temirlan on 29.06.2017.
 */
public class SizeManager {

    private static double TAB_BUTTON_HEIGHT = 76.0;
    private static double TAB_BUTTON_Y_LAYOUT = 5.0;

    public static double getTabButtonYLayout() {
        return TAB_BUTTON_Y_LAYOUT;
    }

    public static void setTabButtonYLayout(double tabButtonYLayout) {
        TAB_BUTTON_Y_LAYOUT = tabButtonYLayout;
    }

    public static double getTabButtonHeight() {
        return TAB_BUTTON_HEIGHT;
    }

    public static void setTabButtonHeight(double tabButtonHeight) {
        TAB_BUTTON_HEIGHT = tabButtonHeight;
    }
}
