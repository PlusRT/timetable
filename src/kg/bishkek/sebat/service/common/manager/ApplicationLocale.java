package kg.bishkek.sebat.service.common.manager;

import kg.bishkek.sebat.service.LocaleService;

import java.util.Locale;

/**
 * Created by temirlan on 23.06.2017.
 */
public class ApplicationLocale implements LocaleService {

    private static ApplicationLocale applicationLocale = new ApplicationLocale();
    private Locale currentLocale;
    private String defaultLocale = "ru";

    private ApplicationLocale() {
        currentLocale = new Locale(defaultLocale);
    }

    public static ApplicationLocale me() {
        return applicationLocale;
    }

    @Override
    public void changeLocale(Locale newLocale) {
        currentLocale = newLocale;
    }

    @Override
    public Locale getLocale() {
        return currentLocale;
    }
}
