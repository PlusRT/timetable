package kg.bishkek.sebat.service.common.manager;

import javafx.fxml.FXMLLoader;
import kg.bishkek.sebat.service.FXMLService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by temirlan on 28.06.2017.
 */
public class FXMLManager implements FXMLService {

    private static List controllers = new ArrayList<>();


    public static <T> T getControllerByClass(String name) {
        try {
            for (Object o : controllers) {
                if (o.getClass().getName().equals(name)) {
                    return (T) o;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("EXCEPPPPTTIONN");
        }
        return null;
    }

    @Override
    public <T> T load(String path) {
        T t = null;
        try {
            FXMLLoader loader = new FXMLLoader();
            t = loader.load(getClass().getResource(path));
            controllers.add(loader.getController());
            return t;
        } catch (Exception e) {
            System.out.println("Error");
            e.printStackTrace();
        }

        return t;
    }

    @Override
    public <T> T load(String path, Locale locale) {
        return null;
    }
}
