package kg.bishkek.sebat.service.common;

import javafx.event.Event;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import kg.bishkek.sebat.controllers.main.MainController;
import kg.bishkek.sebat.util.LoaderUtil;

/**
 * Created by temirlan on 16.06.2017.
 */
public class Common {

    public static void switchTabTo(int index) {
        MainController.getExactController().getTabPane().getSelectionModel().select(index);
    }

    public static Stage getStageWithDefaultConf(String fxml) {
        Parent parent = LoaderUtil.loadFXML(fxml);
        Stage stage = new Stage();
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        return stage;
    }

    public static double getHeight() {
        return 76.0;
    }

    public static void closeWindow(Event event) {
        ((Node) (event.getSource())).getScene().getWindow().hide();
    }
}
