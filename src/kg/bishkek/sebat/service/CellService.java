package kg.bishkek.sebat.service;

import javafx.scene.control.TableColumn;
import kg.bishkek.sebat.domain.Item;

import java.util.List;
import java.util.Map;

/**
 * Created by temirlan on 28.06.2017.
 */
public interface CellService<T> {

    void add(T t);


    T getByIndex(int row, int column);

    T getByColumn(int row, TableColumn column);

    List<T> getAllByItem(Item item);

    Map getCellList();
}
