package kg.bishkek.sebat.service.impl;

import javafx.scene.control.TableCell;
import javafx.scene.input.MouseButton;
import javafx.scene.paint.Color;
import kg.bishkek.sebat.controllers.main.ItemDisplayPaneController;
import kg.bishkek.sebat.domain.Item;
import kg.bishkek.sebat.table.CurrentSelectedItem;

import java.util.Random;

/**
 * Created by temirlan on 21.06.2017.
 */
public class CustomTableCell extends TableCell<String, Item> {

    private Item cellContainer;
    private boolean cellFree;
    private ItemDisplayPaneController display;

    public Item getCellContainter() {
        return cellContainer;
    }

    public void setCellContainer(Item cellContainter) {
        this.cellContainer = cellContainter;
        cellFree = false;
        updateItem(null, false);
    }

    public CustomTableCell(Item item) {
        display = ItemDisplayPaneController.getExactController();
        Random random = new Random();
        attachMouseListener(this);
        cellContainer = new Item(new Color(random.nextDouble(), random.nextDouble(), random.nextDouble(), random.nextDouble()), "Ch");

        setOnMouseEntered(e -> {
            if (display == null) {
                TableCell t = CustomTableCellStore.me().getByColumn(getIndex(), getTableColumn());
                t.setStyle("-fx-background-color: #73AD21");
                return;
            }
            display.display(cellContainer);
        });
        setOnMouseExited(e -> {
            //t.setStyle("-fx-background-color: inherit");
            display.hide();
        });

    }

    public boolean isCellFree() {
        return cellFree;

    }

    public void removeCell() {
        cellContainer = null;
        cellFree = true;
    }

    public void setCellFree(boolean cellFree) {
        this.cellFree = cellFree;
    }

    @Override
    protected void updateItem(Item i, boolean empty) {
        super.updateItem(i, empty);
        if (!empty) {
            if (cellContainer != null) {
                setStyle("-fx-background-color: " + cellContainer.getColor() + ";");
                setText(cellContainer.getShortName());
            } else {
                setStyle("-fx-background-color: inherit");
                setText("");
            }
        }
    }

    private void attachMouseListener(CustomTableCell cell) {
        cell.setOnMouseClicked(e -> {
            CurrentSelectedItem selectedItem = CurrentSelectedItem.me();
            if (e.getButton() == MouseButton.SECONDARY) {
                selectedItem.removeSelected();
                return;
            }
            if (!cell.isEmpty()) {
                if (selectedItem.isSelected()) {
                    CustomTableCell selectedCell = selectedItem.getSelectedCell();
                    if (cell.isCellFree()) {
                        cell.setCellContainer(selectedCell.getCellContainter());
                        selectedItem.removeSelected();
                    } else {
                        selectedItem.setSelectedCell(cell);
                        cell.setCellContainer(selectedCell.getCellContainter());
                    }
                    return;
                }
                if (!cell.isCellFree()) {
                    selectedItem.setSelectedCell(cell);
                    cell.removeCell();
                    System.out.println("free");
                }
            }
        });
    }
}