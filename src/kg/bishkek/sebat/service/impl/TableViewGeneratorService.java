package kg.bishkek.sebat.service.impl;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import kg.bishkek.sebat.service.TableService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by temirlan on 20.06.2017.
 */
public class TableViewGeneratorService implements TableService {

    private List<TableView> tableViews = new ArrayList<>();
    private List<TableColumn> tableColumns = new ArrayList<>();

    @Override
    public List<TableView> getTableViews() {
        return tableViews;
    }

    @Override
    public List<TableColumn> addColumns(TableView tableView, int columnAmount, String... columnsName) {
        List<TableColumn> columns = new ArrayList<>();
        for (int i = 0; i < columnAmount; i++) {
            TableColumn tableColumn = new TableColumn();
            tableColumns.add(tableColumn);
            if (i < columnsName.length) {
                tableColumn.setText(columnsName[i]);
            }
            tableView.getColumns().add(tableColumn);
            columns.add(tableColumn);
        }
        return columns;
    }

    @Override
    public List<TableColumn> addColumns(TableColumn tableColumn, int columnAmount, String... columnsName) {
        List<TableColumn> columns = new ArrayList<>();
        for (int i = 0; i < columnAmount; i++) {
            TableColumn column = new TableColumn();
            if (i < columnsName.length) {
                column.setText(columnsName[i]);
                System.out.print(columnsName[i]);
            }
            tableColumn.getColumns().add(column);
            columns.add(column);
        }
        return columns;
    }


    @Override
    public List<TableColumn> getColumns() {
        return tableColumns;
    }

}
