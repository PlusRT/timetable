package kg.bishkek.sebat.service.impl;

import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import kg.bishkek.sebat.domain.Item;
import kg.bishkek.sebat.service.CellService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by temirlan on 28.06.2017.
 */
public class CustomTableCellStore implements CellService<TableCell> {

    private Map mapByIndex;
    private Map<TableColumn, List<TableCell>> mapByColumn;
    private static CustomTableCellStore store = new CustomTableCellStore();

    private CustomTableCellStore() {
        mapByIndex = new HashMap<>();
        mapByColumn = new HashMap<>();
    }

    @Override
    public void add(TableCell customTableCell) {
        if (!customTableCell.isEmpty()) {
            List list = mapByColumn.get(customTableCell.getTableColumn());
            if (null == list) {
                List<TableCell> temp = new ArrayList<>();
                temp.add(customTableCell);
                mapByColumn.put(customTableCell.getTableColumn(), temp);
                return;
            }
            list.add(customTableCell);
        }
    }

    @Override
    public TableCell getByIndex(int row, int column) {
        return null;
    }

    @Override
    public TableCell getByColumn(int row, TableColumn column) {
        List<TableCell> cells = mapByColumn.get(column);
        if (cells != null) {
            return cells.get(row);
        }
        System.out.println(mapByColumn.size());
        return null;
    }

    @Override
    public List<TableCell> getAllByItem(Item item) {
        return null;
    }

    @Override
    public Map getCellList() {
        return mapByIndex;
    }

    public static CustomTableCellStore me() {
        return store;
    }
}
