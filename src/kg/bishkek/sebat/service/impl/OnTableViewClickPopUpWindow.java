package kg.bishkek.sebat.service.impl;

import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Popup;
import kg.bishkek.sebat.domain.Item;
import kg.bishkek.sebat.service.PopUpWindowService;

/**
 * Created by temirlan on 22.06.2017.
 */
public class OnTableViewClickPopUpWindow implements PopUpWindowService<Item> {

    private TableView tableView;
    private Item item;
    private Popup popup;
    private HBox container;
    private double popUpWidth = 30.0;
    private double popUpHeight = 30.0;

    public OnTableViewClickPopUpWindow(TableView tableView) {
        initConf();
        this.tableView = tableView;
    }

    public OnTableViewClickPopUpWindow(TableView tableView, Item item) {
        this.tableView = tableView;
        initConf();
        setItemOnClick(item);
    }

    private void initConf() {
        container = new HBox();
        popup = new Popup();
        container.setPrefHeight(popUpHeight);
        container.setPrefWidth(popUpWidth);
        popup.getContent().add(container);
    }

    @Override
    public void show() {
        if (!popup.isShowing()) {
            if (item != null) {
                tableView.setOnMouseMoved(e -> {
                    popup.setX(e.getScreenX() - 10);
                    popup.setY(e.getScreenY() + 3);
                });
                tableView.setOnMouseExited(e -> {
                    popup.hide();
                });
                tableView.setOnMouseEntered(e -> {
                    if (!popup.isShowing()) {
                        popup.show(tableView.getScene().getWindow());
                    }
                });
                popup.show(tableView.getScene().getWindow());
                //tableView.setCursor(Cursor.NONE);
            }
        }
    }

    @Override
    public void hide() {
        tableView.setOnMouseClicked(null);
        tableView.setOnMouseExited(null);
        tableView.setOnMouseEntered(null);
        tableView.setCursor(Cursor.DEFAULT);
        popup.hide();
    }

    @Override
    public void setItemOnClick(Item item) {
        container.getChildren().clear();
        this.item = item;
        Text text = new Text(item.getShortName());
        container.setAlignment(Pos.CENTER);
        container.setStyle("-fx-background-color: " + item.getColor());
        container.getChildren().add(text);
    }

    public double getPopUpWidth() {
        return popUpWidth;
    }

    public void setPopUpWidth(double popUpWidth) {
        this.popUpWidth = popUpWidth;
    }

    public double getPopUpHeight() {
        return popUpHeight;
    }

    public void setPopUpHeight(double popUpHeight) {
        this.popUpHeight = popUpHeight;
    }
}
