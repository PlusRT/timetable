package kg.bishkek.sebat.controllers.tabs;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import kg.bishkek.sebat.util.LoaderUtil;

import java.io.IOException;

public class NewTableController {

    enum State {
        TEACHER, CLASSROOM, SUBJECT, CLASS
    }

    private State selectedState = State.TEACHER;
    @FXML
    private Button removeAll;
    @FXML
    private Button generate;
    @FXML
    private Button edit;
    @FXML
    private Button teacher;
    @FXML
    private Button aClass;


    public void buttonPressed(Button button, ActionEvent actionEvent) throws Exception {
    }

    public void teacherButtonPressed(ActionEvent actionEvent) {
        if (selectedState != State.TEACHER) {
            edit.setGraphic(new ImageView(new Image("/resources/skins/toolbar/teacher_16.png")));
            teacher.setGraphic(new ImageView(new Image("/resources/skins/dialogs/Teacher_48_Arrow.png")));
            removeAll.setVisible(false);
            generate.setVisible(false);
            selectedState = State.TEACHER;
        }
    }

    public void roomButtonPressed(ActionEvent actionEvent) {
        if (selectedState != State.CLASSROOM) {
            edit.setGraphic(new ImageView(new Image("/resources/skins/toolbar/room_16.png")));
            generate.setVisible(true);
            removeAll.setVisible(true);
            selectedState = State.CLASSROOM;
        }
    }

    public void classButtonPressed(ActionEvent actionEvent) {
        if (selectedState != State.CLASS) {
            edit.setGraphic(new ImageView(new Image("/resources/skins/toolbar/class_16.png")));
            removeAll.setVisible(true);
            generate.setVisible(false);
            selectedState = State.CLASS;
        }
    }

    public void subjectButtonPressed(ActionEvent actionEvent) {
        if (selectedState != State.SUBJECT) {
            edit.setGraphic(new ImageView(new Image("/resources/skins/toolbar/subject_16.png")));
            generate.setVisible(false);
            removeAll.setVisible(false);
            selectedState = State.SUBJECT;
        }
    }

    public void addNew(ActionEvent event) {
        String pathToResource = "/resources/fxml/wizard/";
        Stage stage = new Stage();
        Scene scene = null;
        switch (selectedState) {
            case TEACHER:
                scene = new Scene(LoaderUtil.loadFXML(pathToResource + "teacher_edit.fxml"));
                break;
            case CLASS:
                scene = new Scene(LoaderUtil.loadFXML(pathToResource + "class_edit.fxml"));
                break;
            case CLASSROOM:
                scene = new Scene(LoaderUtil.loadFXML("/resources/fxml/wizard/classroom_edit.fxml"));
                break;
            case SUBJECT:
                scene = new Scene(LoaderUtil.loadFXML(pathToResource + "subject_edit.fxml"));
                break;
        }
        stage.setScene(scene);
        stage.setTitle(selectedState.name());
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(((Node) event.getSource()).getScene().getWindow());
        stage.show();

    }

    public void showLessonScene(ActionEvent event) {
        Stage stage = new Stage();
        Scene scene = new Scene(LoaderUtil.loadFXML("/resources/fxml/wizard/contract.fxml"));
        stage.setScene(scene);
        stage.setTitle(selectedState.name());
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    public void closeWindow(ActionEvent event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    public void showTimeOff(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent table = FXMLLoader.load(getClass().getResource("../../../../../resources/fxml/time_off.fxml"));
        Scene scene = new Scene(table);
        stage.setScene(scene);
        stage.setTitle("New Table");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    public void setSelectedState(State selectedState) {
        this.selectedState = selectedState;
    }

    private void makeSelected() {
        
    }

}
