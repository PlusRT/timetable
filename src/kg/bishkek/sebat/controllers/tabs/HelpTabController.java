package kg.bishkek.sebat.controllers.tabs;

import javafx.event.Event;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import kg.bishkek.sebat.util.LoaderUtil;

/**
 * Created by temirlan on 19.06.2017.
 */
public class HelpTabController {

    public void changeLanguage(Event event) {
        Stage stage = new Stage();
        Parent parent = LoaderUtil.loadFXML("/resources/fxml/change_lang.fxml");
        Scene scene = new Scene(parent);
        stage.setTitle("Change Language");
        stage.setY(100);
        stage.setX(450);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(((Node) event.getSource()).getScene().getWindow());
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.show();
    }
}
