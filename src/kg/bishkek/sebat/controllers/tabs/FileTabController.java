package kg.bishkek.sebat.controllers.tabs;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import kg.bishkek.sebat.service.common.Common;

import java.io.IOException;

/**
 * Created by temirlan on 11.03.2017.
 */
public class FileTabController {
    public void openContry(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent table = FXMLLoader.load(getClass().getResource("/resources/fxml/time_off.fxml"));
        Scene scene = new Scene(table);
        stage.setScene(scene);
        stage.setTitle("New Table");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    public void switchBack(ActionEvent event) {
        Common.switchTabTo(0);
    }
}
