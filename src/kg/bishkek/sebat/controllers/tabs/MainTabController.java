package kg.bishkek.sebat.controllers.tabs;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.print.*;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SplitMenuButton;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.transform.Scale;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import kg.bishkek.sebat.service.common.Common;
import kg.bishkek.sebat.table.TableController;
import kg.bishkek.sebat.util.FileUtil;
import kg.bishkek.sebat.util.LoaderUtil;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainTabController implements Initializable {

    @FXML
    private SplitMenuButton splitMenuButton;
    @FXML
    private AnchorPane mainAnchorPane;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (splitMenuButton != null) {
            splitMenuButton.setMaxWidth(130);
            splitMenuButton.setMaxHeight(10);
            splitMenuButton.setPrefWidth(splitMenuButton.getMaxWidth());
            splitMenuButton.setPrefHeight(splitMenuButton.getMaxHeight());
        }
    }

    public void generateStage(ActionEvent actionEvent) throws Exception {
        String path = "/resources/fxml/stages/newTable.fxml";
        Stage stage = new Stage();
        NewTableController controller = LoaderUtil.getController(path);
        controller.classButtonPressed(null);
        Parent table = LoaderUtil.loadFXML(path);
        Scene scene = new Scene(table);
        stage.setScene(scene);
        stage.setTitle("New Table");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(((Node) actionEvent.getSource()).getScene().getWindow());
        stage.show();
    }

    public void create(ActionEvent event) throws IOException {
        Stage stage = new Stage();
        Parent table = FXMLLoader.load(getClass().getResource("../../../../../resources/fxml/wizard/wizardMain.fxml"));
        Scene scene = new Scene(table);
        stage.setScene(scene);
        stage.setTitle("New Table");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }

    public void openFile(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File selectedFile = chooser.showOpenDialog(((Node) event.getSource()).getScene().getWindow());
        if (selectedFile != null) {
            boolean fit = FileUtil.doesFileMatchExtension(selectedFile, "pptx");
        }
    }

    public void showPrinterDialog(ActionEvent event) {
        TableView node = TableController.getExactController().getTableView();
        Printer printer = Printer.getDefaultPrinter();
        PageLayout pageLayout = printer.createPageLayout(Paper.A4,
                PageOrientation.LANDSCAPE, Printer.MarginType.DEFAULT);

        PrinterJob job = PrinterJob.createPrinterJob();
        double scaleX = pageLayout.getPrintableWidth() / node.getBoundsInParent().getWidth();
        double scaleY = pageLayout.getPrintableHeight() / node.getBoundsInParent().getHeight();
        node.getTransforms().add(new Scale(scaleX, scaleY));
        if (job != null && job.showPrintDialog(node.getScene().getWindow())) {
            boolean success = job.printPage(node);
            if (success) {
                job.endJob();
            }
        }
    }

    public void backToMain(ActionEvent event) {
        Common.switchTabTo(0);
    }

    public void showSettingsScene(ActionEvent event) {
        Stage stage = new Stage();
        Parent parent = LoaderUtil.loadFXML("/resources/fxml/settings.fxml");
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.setTitle("New Table");
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(((Node) event.getSource()).getScene().getWindow());
        stage.show();
    }


    public void selected(Event event) {
      /*  Node node = ((MenuItem) event.getSource()).getGraphic();
        HBox hBox = (HBox) node;
        hBox.setMaxHeight(splitMenuButton.getMaxHeight()/2);
        hBox.setMaxWidth(splitMenuButton.getMaxWidth());
        hBox.setPrefHeight(splitMenuButton.getHeight() / 2);
        hBox.setPrefWidth(splitMenuButton.getWidth() - 20);
        splitMenuButton.setGraphic(null);
        splitMenuButton.setGraphic(hBox);*/
    }

    public void playVideo(ActionEvent event) {
       /* MediaPlayer video = new MediaPlayer(new Media("/resources/video/lucky.avi"));
        MediaView mediaView = new MediaView();
        mediaView.setMediaPlayer(video);
        ItemDisplayPaneController temp = ItemDisplayPaneController.getExactController();
        temp.hide();
        temp.getContainer().getChildren().add(mediaView);*/
    }

    public void printPreview(ActionEvent event) {
      /*  Printer printer = Printer.getDefaultPrinter();
        PageLayout pageLayout = printer.createPageLayout(Paper.A4, PageOrientation.LANDSCAPE, 0, 0, 0, 0);
        node.setPrefWidth(Paper.A4.getWidth());
        node.setPrefHeight(Paper.A4.getHeight());
        PrinterJob job = PrinterJob.createPrinterJob();
        job.setPrinter(printer);
        if (job != null && job.showPrintDialog(((Node) event.getSource()).getScene().getWindow())) {
            boolean success = job.printPage(pageLayout, node);
            if (success) {
                job.endJob();
            }
        }*/
        AnchorPane parent = LoaderUtil.loadFXML("/resources/fxml/print/print_paper_table.fxml");
        Stage stage = new Stage();
        Scene scene = new Scene(parent);
        stage.setScene(scene);
        stage.setTitle("New Table");
        stage.show();
    }
}
