package kg.bishkek.sebat.controllers.main;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;

public class MainController {

    @FXML
    private TabPane tabPane;
    @FXML
    private AnchorPane mainContainer;

    public static MainController getMainController() {
        return mainController;
    }

    @FXML

    private TableView table;
    @FXML
    private Slider slider;

    private ItemDisplayPaneController display;
    @FXML
    private Separator separator;
    private double displayAnchor = 40;

    private static MainController mainController;

    public void initialize() throws Exception {
        mainController = this;
        AnchorPane p = FXMLLoader.load(getClass().getResource("/resources/fxml/main/mainscreen_subject_display_pane.fxml"));
        mainContainer.getChildren().add(p);
        display = ItemDisplayPaneController.getExactController();
        table.setFixedCellSize(slider.getValue());


        AnchorPane.setBottomAnchor(table, display.getMainPane().getPrefHeight() + displayAnchor);
        AnchorPane.setBottomAnchor(display.getMainPane(), displayAnchor);
        AnchorPane.setTopAnchor(table, tabPane.getPrefHeight()+5);


        slider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> ov,
                                Number old_val, Number new_val) {
                table.setFixedCellSize(new_val.intValue());
            }
        });
    }

    private void setOnDisplayPaneMouseDragged() {

        /*separator.setOnMouseDragged(e -> {
            separator.setLayoutY(e.getY());
        });
        separator.setOnMouseDragExited(e -> {
            display.setLayoutY(separator.getLayoutY());
            AnchorPane.setBottomAnchor(table,display.getPrefHeight()+displayAnchor);
        });*/
    }

    public AnchorPane getMainContainer() {
        return mainContainer;
    }

    public TabPane getTabPane() {
        return tabPane;
    }

    public ItemDisplayPaneController getDisplay() {
        return display;
    }

    public static MainController getExactController() {
        return mainController;
    }
}