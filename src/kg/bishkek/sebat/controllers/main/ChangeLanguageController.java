package kg.bishkek.sebat.controllers.main;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import kg.bishkek.sebat.service.LocaleService;
import kg.bishkek.sebat.service.common.manager.ApplicationLocale;
import kg.bishkek.sebat.service.common.Common;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by temirlan on 19.06.2017.
 */
public class ChangeLanguageController implements Initializable {
    private LocaleService localeService;


    public void closeWindow(ActionEvent event) {
        Common.closeWindow(event);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        localeService = ApplicationLocale.me();
    }

    public void changeLang(ActionEvent event) {
        localeService.changeLocale(new Locale("ru"));
    }
}
