package kg.bishkek.sebat.controllers.main;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Separator;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import kg.bishkek.sebat.domain.Item;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by temirlan on 27.06.2017.
 */
public class ItemDisplayPaneController implements Initializable {

    @FXML
    private Pane itemDisplayPane;
    @FXML
    private AnchorPane display;
    @FXML
    private AnchorPane container;
    private static ItemDisplayPaneController itemDisplayPaneController;
    @FXML
    private Separator separator;


    public Separator getSeparator() {
        return separator;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        for (Node node : container.getChildren()) {
            node.setVisible(false);
        }
        itemDisplayPaneController = this;
        itemDisplayPane.getChildren().add(new Text("Fgvbs"));
        itemDisplayPane.setStyle("-fx-background-color: #b3d8ff");
    }

    public void display(Item item) {
        for (Node node : container.getChildren()) {
            node.setVisible(true);
        }
        itemDisplayPane.setStyle("-fx-background-color: " + item.getColor() + ";");
        itemDisplayPane.getChildren().clear();
        Text text = new Text(item.getShortName());
        text.setTextAlignment(TextAlignment.CENTER);
        itemDisplayPane.getChildren().add(text);
    }

    public void hide() {
        for (Node node : container.getChildren()) {
            node.setVisible(false);
        }
    }


    public AnchorPane getMainPane() {
        return display;
    }

    public static ItemDisplayPaneController getExactController() {
        return itemDisplayPaneController;
    }

    public Pane getItemDisplayPane() {
        return itemDisplayPane;
    }

    public AnchorPane getContainer() {
        return container;
    }
}
