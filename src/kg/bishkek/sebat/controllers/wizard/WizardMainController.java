package kg.bishkek.sebat.controllers.wizard;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import kg.bishkek.sebat.util.LoaderUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by temirlan on 15.06.2017.
 */
public class WizardMainController {

    @FXML
    private AnchorPane pane;
    @FXML
    private Button previous;
    @FXML
    private Button next;
    private int currentPage;
    private Map<Integer, String> fxmlPathes;

    @FXML
    public void initialize() {
        fillFxmlPath();
        setScene(currentPage);
        previous.setDisable(true);
    }


    private void fillFxmlPath() {
        fxmlPathes = new HashMap<>();
        fxmlPathes.put(0, "/resources/fxml/wizard/creation.fxml");
        fxmlPathes.put(1, "/resources/fxml/stages/newTable.fxml");
        fxmlPathes.put(2, "/resources/fxml/wizard/choosing.fxml");
        fxmlPathes.put(3, "/resources/fxml/wizard/wizard_last_step.fxml");
    }


    public void setNextScene(ActionEvent event) {
        int temp = currentPage++;
        setScene(temp);
        previous.setDisable(false);
    }

    public void setPreviousScene(ActionEvent event) {
        int temp = currentPage--;
        setScene(temp);
        next.setText("Next");
    }

    private void setScene(int temp) {
        if (checkCurrentScene()) {
            AnchorPane anchorPane = LoaderUtil.loadFXML(fxmlPathes.get(currentPage));
            pane.getChildren().clear();
            pane.getChildren().add(anchorPane);
            return;
        }
        currentPage = temp;
    }

    private boolean checkCurrentScene() {
        if (currentPage >= fxmlPathes.size()) {
            return false;
        }
        if (currentPage == fxmlPathes.size() - 1) {
            next.setText("Ready");
        }
        if (currentPage < 0) {
            previous.setDisable(true);
            return false;
        }
        if (currentPage == 0) {
            previous.setDisable(true);
        }
        return true;
    }

    public void closeStage(ActionEvent event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }


}
