package kg.bishkek.sebat.controllers.wizard;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;

public class WizardStepTwoController {

    private int layoutSpacing = 22;
    private int paneSpacing = 91;
    private int paneAmount = 16;
    private double panePrefHeight = 84.0;
    private int local;
    @FXML
    private AnchorPane anchorPane;

    @FXML
    public void initialize() throws Exception {
        try {
            int indexing = 0;
            Pane[] panes = generatePanes(paneAmount);
            List<String> path = getImageURLs();
            for (Pane pane : panes) {
                if (indexing != 0) {
                    setWizardHyperlinks(pane, "Yes", "No", "Not sure");
                } else {
                    setWizardHyperlinks(pane, "Subject", "Course", "Not sure");
                }
                setWizardText(pane, "this is the tittle or description ");
                addPaneImage(pane, path.get(indexing));
                anchorPane.getChildren().add(pane);
                indexing++;
            }
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error occurred");
            alert.setHeaderText(e.getMessage());
            alert.showAndWait();
        }
    }

    private List<String> getImageURLs() {
        List<String> list = new ArrayList<>();
        String imagePath = "/resources/skins/clean/";
        list.add(imagePath + "subject_64.png");
        list.add(imagePath + "Classroom_64.png");
        list.add(imagePath + "Groups_64.png");
        list.add(imagePath + "Checklist_64.png");
        list.add(imagePath + "building_64.png");
        list.add(imagePath + "weeks_64.png");
        list.add(imagePath + "Saturday_64.png");
        list.add(imagePath + "terms_64.png");
        list.add(imagePath + "consecutively_64.png");
        list.add(imagePath + "2nd_period_64.png");
        list.add(imagePath + "same_period_64.png");
        list.add(imagePath + "gaps_64.png");
        list.add(imagePath + "teacher_gaps_64.png");
        list.add(imagePath + "0thPeriod_64.png");
        list.add(imagePath + "2shifts_64.png");
        list.add(imagePath + "2plus1_64.png");
        return list;
    }

    private void setWizardHyperlinks(Pane pane, String... names) {
        double checkBoxLayoutX = 408.0;
        double checkBoxPrefWidth = 80.0;
        double checkBoxLayoutY = 5.0;
        double checkBoxPrefHeight = 16.0;
        ToggleGroup group = new ToggleGroup();
        for (String s : names) {
            RadioButton check = new RadioButton();
            check.setToggleGroup(group);
            check.setSelected(false);
            check.setText(s);
            check.setLayoutX(checkBoxLayoutX);
            check.setPrefWidth(checkBoxPrefWidth);
            check.setPrefHeight(checkBoxPrefHeight);
            check.setLayoutY(checkBoxLayoutY);
            pane.getChildren().addAll(check);
            checkBoxLayoutY += layoutSpacing;
        }
    }

    private void addPaneImage(Pane pane, String imagePath) throws Exception {
        double imageLayoutX = 7.0;
        double imagePrefHeight = 64.0;
        double imagePrefWidth = 64.0;
        double imageSpacing = 10;
        ImageView imageView = new ImageView();
        Image image = new Image(imagePath);
        imageView.setImage(image);
        imageView.setLayoutY(imageSpacing);
        imageView.setLayoutX(imageLayoutX);
        imageView.setFitHeight(imagePrefHeight);
        imageView.setFitWidth(imagePrefWidth);
        pane.getChildren().add(imageView);

    }

    private void setWizardText(Pane pane, String text) {
        double textLayoutX = 139.0;
        double wrappingWidth = 200;
        int textSpacing = 25;
        Text t = new Text();
        t.setLayoutX(textLayoutX);
        t.setLayoutY(t.getLayoutY() + textSpacing);
        t.setStrokeType(StrokeType.OUTSIDE);
        t.setWrappingWidth(wrappingWidth);
        t.setText(text);
        pane.getChildren().add(t);
    }

    private Pane[] generatePanes(int paneAmount) {
        double paneLayoutX = 5.0;
        double paneLayoutY = 0;
        double panePrefWidth = 780.0;

        Pane[] panes = new Pane[paneAmount];
        for (int i = 0; i < paneAmount; i++) {
            Pane pane = new Pane();
            pane.setLayoutY(paneLayoutY);
            pane.setLayoutX(paneLayoutX);
            pane.setPrefHeight(panePrefHeight);
            pane.setPrefWidth(panePrefWidth);
            paneLayoutY += paneSpacing;
            panes[i] = pane;
            pane.setStyle("-fx-border-color: #e5d4da;");
        }
        return panes;
    }
}
