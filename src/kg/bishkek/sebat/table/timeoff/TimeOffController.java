package kg.bishkek.sebat.table.timeoff;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import kg.bishkek.sebat.service.impl.TableViewGeneratorService;

import java.util.ArrayList;
import java.util.List;

public class TimeOffController {

    private TableViewGeneratorService tableViewService;
    private TableView tableView;
    private ObservableList<Day> days;
    @FXML
    private AnchorPane container;
    private List<ImageView> pictures;
    private String ok = "/resources/skins/actions/ok_32.png";
    private String no = "/resources/skins/actions/no_32.png";
    private String help = "/resources/skins/actions/help_32.png";


    public void initialize() {
        initList();
        confTable();
        container.getChildren().add(tableView);
    }

    private void confTable() {
        double tableViewLayoutX = 27.0;
        double tableViewLayoutY = 31.0;
        double tablePrefHeight = 265.0;
        double tablePrefWidth = 584;

        tableViewService = new TableViewGeneratorService();
        tableView = new TableView();
        tableViewService.addColumns(tableView, 8, "", "1", "2", "3", "4", "5", "6", "7");
        for (TableColumn<Day, String> tableColumn : tableViewService.getColumns()) {
            tableColumn.setPrefWidth(68.0);
            tableColumn.setResizable(false);
            tableColumn.setSortable(false);
            tableColumn.setMaxWidth(68.0);
            setListeners(tableColumn);
        }
        tableView.setLayoutX(tableViewLayoutX);
        tableView.setLayoutY(tableViewLayoutY);
        tableView.setPrefHeight(tablePrefHeight);
        tableView.setPrefWidth(tablePrefWidth);
        tableView.setItems(days);
        tableView.getStylesheets().add("/resources/style/table.css");
    }

    private void setListeners(TableColumn tableColumn) {
        tableColumn.setCellValueFactory(new PropertyValueFactory<>("image"));
        tableColumn.setCellFactory(column -> {
            return new TableCell<Day, String>() {
                private int current = 1;

                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if (!empty) {
                        setGraphic(new ImageView(new Image(ok)));
                        this.setOnMouseClicked((MouseEvent e) -> {
                            if (current > 3) {
                                current = 1;
                            }
                            if (current == 1) {
                                setGraphic(new ImageView(new Image(no)));
                            } else if (current == 2) {
                                setGraphic(new ImageView(new Image(help)));
                            } else if (current == 3) {
                                setGraphic(new ImageView(new Image(ok)));
                            }
                            current++;
                        });
                    }
                }
            };
        });
    }

    public void initList() {
        List<Day> l = new ArrayList<>();
        pictures = new ArrayList<>();
        l.add(new Day("Mo"));
        l.add(new Day("Tu"));
        l.add(new Day("We"));
        l.add(new Day("Th"));
        l.add(new Day("Fr"));

        pictures.add(new ImageView(new Image("/resources/skins/actions/ok_32.png")));
        pictures.add(new ImageView(new Image("/resources/skins/actions/no_32.png")));
        pictures.add(new ImageView(new Image("/resources/skins/actions/help_32.png")));
        days = FXCollections.observableArrayList(l);
    }

    public void setForMore(ActionEvent event) {
        tableView.setCursor(Cursor.DISAPPEAR);
        System.out.println("come");
    }

}
