package kg.bishkek.sebat.table.timeoff;

/**
 * Created by temirlan on 18.06.2017.
 */
public class Day {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Day(String name) {

        this.name = name;
    }
}
