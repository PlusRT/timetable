package kg.bishkek.sebat.table;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import kg.bishkek.sebat.domain.Item;
import kg.bishkek.sebat.service.impl.CustomTableCell;
import kg.bishkek.sebat.service.impl.CustomTableCellStore;
import kg.bishkek.sebat.service.impl.TableViewGeneratorService;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by temirlan on 11.03.2017.
 */
public class TableController implements Initializable {

    @FXML
    private TableView tableView;
    private TableViewGeneratorService tableViewService;
    private ObservableList<Subject> list;

    public static TableController getExactController() {
        return exactController;
    }

    private CustomTableCellStore store;
    private static TableController exactController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        exactController = this;
        generateTable();
        initList();
        tableView.setItems(list);
        store = CustomTableCellStore.me();
    }

    private void initList() {
        int classess = 11;
        list = FXCollections.observableArrayList();
        for (int i = 0; i < classess; i++) {
            list.add(new Subject());
        }
    }

    private void generateTable() {
        CurrentSelectedItem.me().setTableView(tableView);
        //   double dayPrefWidth = 291;
        // double hourPrefWidth = 40.0;
        String[] days = {"Monday", "Tuesday", "Wednesday", "Friday", "Saturday"};
        String[] hours = {"1", "2", "3", "4", "5", "6", "7"};
        tableViewService = new TableViewGeneratorService();
        TableColumn dayColumn = new TableColumn();
        //  dayColumn.setResizable(false);
        tableView.getColumns().add(dayColumn);
        tableViewService.addColumns(tableView, days.length, days);
        for (TableColumn day : tableViewService.getColumns()) {
            day.setSortable(false);
            day.getStyleClass().add("dayColumn");
            for (TableColumn h : tableViewService.addColumns(day, hours.length, hours)) {
                h.setSortable(false);
                setListener(h);
                h.getStyleClass().add("hourColumn");
            }
        }
        dayColumn.setCellValueFactory(new PropertyValueFactory<Subject, Text>("text"));
        dayColumn.setCellFactory(col -> {
            TableCell s = new TableCell() {
                @Override
                protected void updateItem(Object item, boolean empty) {
                    super.updateItem(item, empty);
                    if (!empty) {
                        setText(item.toString());
                        store.add(this);
                    }
                }
            };
            return s;
        });
    }

    private void setListener(TableColumn column) {
        column.setCellFactory(col -> {
            CustomTableCell cell = new CustomTableCell(new Item(Color.YELLOW, "Ch"));
            store.add(cell);
            return cell;
        });
    }

    public TableView getTableView() {
        return tableView;
    }
}