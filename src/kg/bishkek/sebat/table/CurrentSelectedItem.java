package kg.bishkek.sebat.table;

import javafx.scene.control.TableView;
import kg.bishkek.sebat.service.impl.CustomTableCell;
import kg.bishkek.sebat.service.impl.OnTableViewClickPopUpWindow;

/**
 * Created by temirlan on 21.06.2017.
 */
public class CurrentSelectedItem {

    private static CurrentSelectedItem currentSelectedItem = new CurrentSelectedItem();
    private boolean selected = true;
    private CustomTableCell selectedCell;
    private OnTableViewClickPopUpWindow popUpWindow;

    public static CurrentSelectedItem getCurrentSelectedItem() {
        return currentSelectedItem;
    }

    public static void setCurrentSelectedItem(CurrentSelectedItem selectedItem) {
        currentSelectedItem = selectedItem;
    }

    public CustomTableCell getSelectedCell() {
        return selectedCell;
    }

    public void setSelectedCell(CustomTableCell cell) {
        if (cell != null) {
            this.selectedCell = cell;
            selected = true;
            popUpWindow.setItemOnClick(cell.getCellContainter());
            popUpWindow.show();
            System.out.println(cell.getCellContainter().getShortName());
        }
    }

    private CurrentSelectedItem() {
    }

    public static CurrentSelectedItem me() {
        return currentSelectedItem;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setTableView(TableView tableView) {
        popUpWindow = new OnTableViewClickPopUpWindow(tableView);
    }

    public void removeSelected() {
        selectedCell = null;
        selected = false;
        popUpWindow.hide();
    }


}
