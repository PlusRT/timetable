package kg.bishkek.sebat.table;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.ArrayList;
import java.util.List;

public class TableColumnService<E, T> {

    public static final double TABLE_PREF_HEIGHT = 469.0;
    public static double TABLE_PREF_WIDTH = 1409.0;

    private TableView<T> tableView;
    private List<TableColumn<E, T>> days;
    private List<TableColumn<E, T>> hours;
    private final int SIDE_PADDING_COLUMN = 2;
    private final double TOP_PADDING_TABLE = 121.0;

    public List<TableColumn<E, T>> getHours() {
        return hours;
    }

    public TableView getTableView() {
        return tableView;
    }

    public List<TableColumn<E, T>> getDays() {
        return days;
    }

    public TableColumnService() {
        tableView = new TableView<T>();
        tableView.setPrefHeight(this.TABLE_PREF_HEIGHT);
        tableView.setPrefWidth(this.TABLE_PREF_WIDTH);
        tableView.setLayoutY(this.TOP_PADDING_TABLE);
        tableView.setFocusTraversable(false);
        days = new ArrayList<>();
        hours = new ArrayList<>();
        tableView.setSelectionModel(null);
        tableView.setOnMouseClicked(new TableViewListener());
    }

    public TableView createTable(List<String> dayNames, int hoursNumber) {
        double dayColumnWidth = TABLE_PREF_WIDTH / dayNames.size();
        double hourColumnWidth = dayColumnWidth / hoursNumber - this.SIDE_PADDING_COLUMN;
        for (String dayTitle : dayNames) {
            TableColumn dayColumn = columnGenerator(dayTitle, dayColumnWidth);
            days.add(dayColumn);
            for (int lessonNO = 1; lessonNO <= hoursNumber; lessonNO++) {
                TableColumn hourColumn = columnGenerator(String.valueOf(lessonNO), hourColumnWidth);
                dayColumn.getColumns().add(hourColumn);
                hours.add(hourColumn);
            }
            tableView.getColumns().add(dayColumn);
        }
        System.out.print(hours.size());
        return tableView;
    }

    private TableColumn columnGenerator(String text, double prefWidth) {
        TableColumn tableColumn = new TableColumn();
        tableColumn.setSortable(false);
        tableColumn.setResizable(false);
        tableColumn.setText(text);
        tableColumn.setPrefWidth(prefWidth);
        return tableColumn;
    }
}
