package kg.bishkek.sebat.table;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;


public class Subject {
    private Text text;
    private TableView table;

    public TableView getTable() {
        return table;
    }

    public Text getText() {
        return text;
    }

    public void setText(Text text) {
        this.text = text;
    }

    public Subject() {
        fill();
        text = new Text("MO");
    }

    private void fill() {
        table = new TableView();
        for (int i = 0; i < 5; i++) {
            TableColumn col = new TableColumn();
            col.setCellValueFactory(new PropertyValueFactory<Subject, Text>("text"));
            col.setCellFactory(c -> {
                return new TableCell() {
                    @Override
                    protected void updateItem(Object item, boolean empty) {
                        super.updateItem(item, empty);
                        setStyle("-fx-background-color: red");
                    }
                };
            });
            table.getColumns().add(col);
        }
        ObservableList<String> s = FXCollections.observableArrayList();
        s.add("sf");
        s.add("sf");
        s.add("sf");
        table.setItems(s);
    }

}
