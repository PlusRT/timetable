package kg.bishkek.sebat.table;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class SubjectObservableList {

    private ObservableList<Subject> subjects = FXCollections.observableArrayList();

    public void addSubject(Subject subject) {
        this.subjects.add(subject);
    }

    public void fill() {
        for (int i = 0; i < 3; i++) {
            subjects.add(new Subject());
        }
    }
    public ObservableList getList() {
        return subjects;
    }
}
