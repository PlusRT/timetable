package kg.bishkek.sebat.util;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;

/**
 * Created by temirlan on 16.06.2017.
 */
public class LoaderUtil {
    public static <T> T loadFXML(String path) {
        T t = null;
        try {
            t = FXMLLoader.load(LoaderUtil.class.getResource(path));
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.getMessage());
            alert.setTitle("ERROR");
            alert.showAndWait();
            e.printStackTrace();
        }
        return t;
    }

    public static <T> T getController(String path) {
        T t = null;
        try {
            FXMLLoader loader = new FXMLLoader(LoaderUtil.class.getResource(path));
            loader.load();
            t = loader.getController();

        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(e.getMessage());
            alert.setTitle("ERROR");
            alert.showAndWait();
        }
        return t;
    }
}
