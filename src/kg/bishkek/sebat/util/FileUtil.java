package kg.bishkek.sebat.util;

import java.io.File;

/**
 * Created by temirlan on 13.06.2017.
 */
public class FileUtil {

    public static String getExtension(File file) {
        String name = file.getName();
        try {
            return name.substring(name.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }

    }

    public static boolean doesFileMatchExtension(File file, String mime) {
        String name = file.getName();
        try {
            if (name.substring(name.lastIndexOf(".") + 1).equals(mime)) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }
}
