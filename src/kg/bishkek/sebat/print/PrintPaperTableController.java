package kg.bishkek.sebat.print;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import kg.bishkek.sebat.service.impl.TableViewGeneratorService;
import kg.bishkek.sebat.table.Subject;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Created by temirlan on 29.06.2017.
 */
public class PrintPaperTableController implements Initializable {

    @FXML
    private Text dateText;
    @FXML
    private TableView tableView;
    private ObservableList<Subject> list;
    private TableViewGeneratorService service;
    private double cellSize;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        list = FXCollections.observableArrayList();
        service = new TableViewGeneratorService();
        fillList();
        addColumns();
        Date date = new Date();
        dateText.setText(dateText.getText() + date.toString());
    }

    private void fillList() {
        for (int i = 0; i < 6; i++) {
            list.add(new Subject());
        }
    }

    private void addColumns() {
        String[] hours = {" ", "1", "2", "3", "4", "5"};
        for (TableColumn column : service.addColumns(tableView, 6)) {
            setListeners(column);
            column.getStyleClass().add("table-column");
            column.setSortable(false);
        }
        tableView.setItems(list);
        tableView.setFixedCellSize(25);
        //   tableView.prefHeightProperty().bind(tableView.fixedCellSizeProperty().multiply(list.size()+1));
        tableView.fixedCellSizeProperty().bind(tableView.prefHeightProperty().divide(list.size() + 1));
        tableView.minHeightProperty().bind(tableView.prefHeightProperty());
        tableView.maxHeightProperty().bind(tableView.prefHeightProperty());
    }

    private void setListeners(TableColumn column) {
        column.setCellFactory(new PropertyValueFactory<Subject, Text>("text"));
        VBox vBox = new VBox();
        vBox.getChildren().addAll(new Text("fg"), new Text("gfd"));
        column.setGraphic(vBox);
        column.setCellFactory(col -> {
            TableCell cell = new TableCell() {
                @Override
                protected void updateItem(Object item, boolean empty) {
                    super.updateItem(item, empty);
                    if (!empty) {
                        AnchorPane pane = new AnchorPane();
                        Text t = new Text("dfv");
                        Text t2 = new Text("svd");
                        t.setLayoutX(20);
                        t.setLayoutY(20);

                        t2.setLayoutX(60);
                        t2.setLayoutY(40);

                        pane.getChildren().addAll(t, t2);
                        setGraphic(pane);
                    }
                }
            };
            return cell;
        });
    }
}
