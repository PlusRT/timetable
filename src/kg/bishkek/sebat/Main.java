package kg.bishkek.sebat;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import kg.bishkek.sebat.controllers.main.MainController;
import kg.bishkek.sebat.service.LocaleService;
import kg.bishkek.sebat.service.common.manager.ApplicationLocale;

import java.util.ResourceBundle;

public class Main extends Application {

    private static MainController mainController;
    private LocaleService localeService = ApplicationLocale.me();

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/resources/fxml/main.fxml"));
        loader.setResources(ResourceBundle.getBundle("resources.locale.Tab", localeService.getLocale()));
        Parent tabs = loader.load();
        primaryStage.setTitle(loader.getResources().getString("title"));
        MainController controller = loader.getController();
        primaryStage.getIcons().add(new Image("/resources/pictures/sebat.png"));
        mainController = controller;
        Scene scene = new Scene(tabs);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static MainController getMainController() {
        return mainController;
    }

    public static void main(String[] args) {
        launch(args);
    }
}
