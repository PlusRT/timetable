package kg.bishkek.sebat.domain;

import javafx.scene.paint.Color;
import kg.bishkek.sebat.util.FxUtils;

/**
 * Created by temirlan on 21.06.2017.
 */
public class Item {
    private Color color;
    private String shortName;

    public String getColor() {
        return FxUtils.toRGBCode(color);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Item(Color color, String shortName) {
        this.color = color;
        this.shortName = shortName;
    }

    public String getShortName() {
        return shortName;

    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
