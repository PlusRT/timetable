package kg.bishkek.sebat.domain;

import javafx.scene.paint.Color;

/**
 * Created by temirlan on 15.06.2017.
 */
public class ClassRoom {
    private String name;
    private String shortName;
    private boolean homeClassRoom;
    private boolean sharedRoom;
    private boolean requiresSupervision;
    private Color color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public boolean isHomeClassRoom() {
        return homeClassRoom;
    }

    public void setHomeClassRoom(boolean homeClassRoom) {
        this.homeClassRoom = homeClassRoom;
    }

    public boolean isSharedRoom() {
        return sharedRoom;
    }

    public void setSharedRoom(boolean sharedRoom) {
        this.sharedRoom = sharedRoom;
    }

    public boolean requiresSupervision() {
        return requiresSupervision;
    }

    public void setRequiresSupervision(boolean requiresSupervision) {
        this.requiresSupervision = requiresSupervision;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
