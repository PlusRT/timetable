package kg.bishkek.sebat.domain;

import javafx.scene.paint.Color;


/**
 * Created by temirlan on 15.06.2017.
 */
public abstract class Unit {
    protected String firstName;
    protected String lastName;
    protected String shortName;
    protected String email;
    protected String phone;
    protected String tittle;
    protected Color color;
    protected boolean gender;
    protected Class aClass;
}
