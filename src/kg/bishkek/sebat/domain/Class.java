package kg.bishkek.sebat.domain;

import javafx.scene.paint.Color;

/**
 * Created by temirlan on 15.06.2017.
 */
public class Class {
    private String name;
    private String shortName;
    private Teacher classTeacher;
    private Color color;
    private boolean printSubjectPictures;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Teacher getClassTeacher() {
        return classTeacher;
    }

    public void setClassTeacher(Teacher classTeacher) {
        this.classTeacher = classTeacher;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isPrintSubjectPictures() {
        return printSubjectPictures;
    }

    public void setPrintSubjectPictures(boolean printSubjectPictures) {
        this.printSubjectPictures = printSubjectPictures;
    }
}
